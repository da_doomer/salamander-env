# salamander-env

Python adaptation of [`salamander.wbt`](https://www.cyberbotics.com/doc/guide/salamander?version=R2021a), the Webots
implementation of a salamander-like robot developed by the EPFL BioRob laboratory.

## Dependencies

You should install [Webots](https://en.wikipedia.org/wiki/Webots), from [Cyberbotics](https://cyberbotics.com/).
The official installation instructions are currently hosted in the [Webots User Guide](https://cyberbotics.com/doc/guide/installing-webots).

Note that [2023 version of Webots have a bug in ElevationGrid](https://github.com/cyberbotics/webots/pull/6412), so make sure you use a 2024 version.
