"""Adapted from salamander.c

https://github.com/cyberbotics/webots/blob/e3a5f1f4ed0deee0e0d51969e1e8b3e6a33a2ad9/projects/samples/rendering/controllers/salamander/salamander.c#L4

At each controller step, the following data structure is print out to
stdout:

```
{
    observation: [float values],
    mode: ("WALK"|"SWIM"),
} ``` """
from controller import Supervisor
import os
import math
import json
from pathlib import Path
import random

# locomotion types
WALK = 0
SWIM = 1

X = 0
Y = 1
Z = 2


# Set this variable to whatever directory you want the animation
# to be saved.
output_dir = ""
output_dir = Path(output_dir)


def clamp(value: float, minv: float, maxv: float) -> float:
    if value < minv:
        return minv
    elif value > maxv:
        return maxv
    return value


def main():
    # Create a temporary directory
    animation_path = output_dir/"animation.html"

    # 6 actuated body segments and 4 legs
    NUM_MOTORS = 10

    # must be the same as in salamander_physics.c
    WATER_LEVEL = 0.05

    # virtual time between two calls to the run() function
    CONTROL_STEP = 32

    robot = Supervisor()
    robot.animationStartRecording(str(animation_path))

    WALK_FREQUENCY = 2.4  # locomotion frequency [Hz]
    SWIM_FREQUENCY = 1.4  # locomotion frequency [Hz]
    WALK_AMPL = 0.1  # radians
    SWIM_AMPL = 1.0  # radians

    # global variables
    spine_offset = 0.0
    ampl = 1.0
    phase = 0.0  # current locomotion phase

    # body and leg motors
    target_position = [0.0 for _ in range(NUM_MOTORS)]

    # get the motors device tags
    MOTOR_NAMES = [
        "motor_1",
        "motor_2",
        "motor_3",
        "motor_4",
        "motor_5",
        "motor_6",
        "motor_leg_1",
        "motor_leg_2",
        "motor_leg_3",
        "motor_leg_4",
    ]
    motor = [
        robot.getDevice(motor_name)
        for motor_name in MOTOR_NAMES
    ]
    min_motor_position = [
        motor_i.getMinPosition()
        for motor_i in motor
    ]
    max_motor_position = [
        motor_i.getMaxPosition()
        for motor_i in motor
    ]

    # get and enable left and right distance sensors
    ds_left = robot.getDevice("ds_left")
    ds_left.enable(CONTROL_STEP)
    ds_right = robot.getDevice("ds_right")
    ds_right.enable(CONTROL_STEP)

    # get and enable gps device
    gps = robot.getDevice("gps")
    gps.enable(CONTROL_STEP)

    # Helper function to get the robots observation
    def get_observation():
        # This is not the actual motor position, but it's close enough
        motor_positions = target_position
        gps_values = gps.getValues()
        left_val = ds_left.getValue()
        right_val = ds_right.getValue()
        observation = [
            *motor_positions,
            left_val,
            right_val,
            gps_values[Z]
        ]
        return observation

    max_steps = 1000

    # control loop: sense-compute-act
    locomotion = WALK
    step_i = 0
    mode = WALK
    while robot.step(CONTROL_STEP) != -1:
        if step_i >= max_steps:
            break
        step_i += 1

        # Store observation
        observation = get_observation()

        # perform sensor measurement
        left_val = ds_left.getValue()
        right_val = ds_right.getValue()

        # change direction according to sensor reading
        spine_offset = (right_val - left_val)

        # increase phase according to elapsed time
        if locomotion == SWIM:
            frequency = SWIM_FREQUENCY
        else:
            frequency = WALK_FREQUENCY
        phase -= CONTROL_STEP / 1000.0 * frequency * 2.0 * math.pi

        # get current elevation from gps
        elevation = gps.getValues()[Z]

        if locomotion == SWIM and elevation > WATER_LEVEL - 0.003:
            locomotion = WALK
            phase = target_position[6]
        elif locomotion == WALK and elevation < WATER_LEVEL - 0.015:
            locomotion = SWIM

        # Randomly change locomotion modes with small probability
        if random.random() < 1/200:
            if locomotion == SWIM:
                locomotion = WALK
            else:
                locomotion = SWIM

        # switch locomotion control according to current robot elevation
        # and water level
        if locomotion == WALK:
            # above water level: walk (s-shape of robot body)

            A = [-0.7, 1, 1, 0, -1, -1]
            for i in range(6):
                target_position[i] = WALK_AMPL * ampl * A[i] * math.sin(phase) + spine_offset

            # rotate legs
            target_position[6] = phase
            target_position[7] = phase + math.pi
            target_position[8] = phase + math.pi
            target_position[9] = phase
        else:  # SWIM
            # below water level: swim (travelling wave of robot body)
            for i in range(6):
                target_position[i] = SWIM_AMPL * ampl * math.sin(phase + i * (2 * math.pi / 6)) * ((i + 5) / 10.0) + spine_offset

            # put legs backwards for swimming
            backwards_position = phase - math.fmod(phase, 2.0 * math.pi) - math.pi / 2
            for i in range(6, NUM_MOTORS):
                target_position[i] = backwards_position

        # motors actuation
        for i in range(NUM_MOTORS):
            # Leg motors don't have max and min positions, and the
            # value should not be clamped. This magic code checks for that
            # case.
            minv = min_motor_position[i]
            maxv = max_motor_position[i]
            if not (minv == 0.0 and maxv == 0.0):
                target_position[i] = clamp(
                    target_position[i],
                    min_motor_position[i],
                    max_motor_position[i],
                )
            motor[i].setPosition(target_position[i])

        # Print observation, action and mode
        if mode == SWIM and elevation > WATER_LEVEL - 0.003:
            mode = WALK
        elif mode == WALK and elevation < WATER_LEVEL - 0.015:
            mode = SWIM
        action = target_position
        status = dict(
            observation=observation,
            action=action,
            mode=mode,
            step_i=step_i,
        )
        status_str = json.dumps(status)
        print(status_str)

    # Stop animation and save viewing instructions
    robot.animationStopRecording()
    animation_instructions = (
        "To view this animation `cd` to this directory and then run `python3 -m http.server`. "
        "Then, open `http://0.0.0.0:8000/animation.html` in your browser."
    )
    animation_instructions_path = output_dir/"README.md"
    with open(animation_instructions_path, "wt") as fp:
        fp.write(animation_instructions)

    # Quit simulation
    robot.simulationQuit(os.EX_OK)

main()
