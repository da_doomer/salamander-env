from gymnasium.envs.registration import register


register(
    id="Salamander-v0",
    entry_point="salamander_env.salamander_gym:SalamanderEnv",
    max_episode_steps=1000,
)
