This directory contains template files to generate maps with custom elevation
grids. The script that generates the maps is not in this directory, but in
`/map_parser.py`.
